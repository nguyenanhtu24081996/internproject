<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/catelogy.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    @include('admin.layout.header') 
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="catelogy">
                    <p>Thời sự</p>
                </div>
            </div>
        </div>
        <div class="top-cate">
            <div class="row">
                <div class="col-lg-6">
                    <div class="first-topic">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/DSCF8205-1583237221_500x300.jpg">
                        <div class="first-topic-detail">
                            <p>Sơn Lôi trước giờ cách ly</p>
                            <div class="first-topic-title">
                                <p>VĨNH PHÚC-Hơn 10.000 người dân xã Sơn Lôi dần trở lại nhịp sinh hoạt bình thường sau 20 ngày bị "phong toả".</p>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="small-topic">
                        <div class="small-scroll">
                            <div class="small-title">
                                <a href="#"><p>TP HCM và 8 tỉnh giả định 30.000 người nhiễm nCoV</p></a>
                            </div>
                            <div class="small-detail">
                                <p>Bộ Tư lệnh Quân khu 7, Bộ Tư lệnh TP HCM và BCH Quân sự 8 tỉnh diễn tập phòng chống dịch Covid-19 nếu có 3.000-30.000 nhiễm bệnh.</p>
                            </div>
                            <hr>
                        </div>
                        <div class="small-scroll">
                            <div class="small-title">
                                <a href="#"><p>TP HCM và 8 tỉnh giả định 30.000 người nhiễm nCoV</p></a>
                            </div>
                            <div class="small-detail">
                                <p>Bộ Tư lệnh Quân khu 7, Bộ Tư lệnh TP HCM và BCH Quân sự 8 tỉnh diễn tập phòng chống dịch Covid-19 nếu có 3.000-30.000 nhiễm bệnh.</p>
                            </div>
                            <hr>
                        </div>
                        <div class="small-scroll">
                            <div class="small-title">
                                <a href="#"><p>TP HCM và 8 tỉnh giả định 30.000 người nhiễm nCoV</p></a>
                            </div>
                            <div class="small-detail">
                                <p>Bộ Tư lệnh Quân khu 7, Bộ Tư lệnh TP HCM và BCH Quân sự 8 tỉnh diễn tập phòng chống dịch Covid-19 nếu có 3.000-30.000 nhiễm bệnh.</p>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="mid-cate">
                    <div class="mid-cate-title">
                        <a href="#"><p>Cần 1.490 tỷ đồng tăng kết nối cho Metro Số 2</p></a>
                    </div>
                    <div class="mid-cate-img">
                        <img src="https://i-vnexpress.vnecdn.net/2020/03/03/metroso2-kgoa-1583234160-8806-1583234258.jpg">
                        <div class="mid-cate-detail">
                            <p>TP HCM Quanh 10 nhà ga tuyến Metro Bến Thành - Tham Lương sẽ được đầu tư hạ tầng, bến bãi, cải tạo vỉa hè, cây xanh... với kinh phí 1.489 tỷ đồng để người dân dễ tiếp cận.</p>
                        </div>
                    </div>
                </div>
                <div class="pagination">
                    <ul class="pagination">
                        <li class="page-item disabled"><a class="page-link" href="#"><</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layout.footer') 
</body>
</html>