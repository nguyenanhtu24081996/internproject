<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Đăng nhập hệ thống</title>
	<link rel="stylesheet" href="../public/css/login.css">
	<link rel="short-cut icon" href="../public/image/image_dev_dung/images.png" />

</head>
<body>
	<div class="container">
		<div class="container-main">
			<div class="main">
				<h2 >ĐĂNG NHẬP</h2>
				<div class="social">
					<a href="#">Login with facebook</a>
				</div>

				<div class="layout-main">
					<h5>(or)</h5>
					<form method="post">
						<input type="email" placeholder="Nhập Email ..." required="">
						<input type="password" placeholder="Nhập mật khẩu..." required="">

						<input type="submit" value="ĐĂNG NHẬP NGAY">
						<a href="#"><h4>Forget password?</h4></a>
					</form>
				</div>
				
			</div>
			<div class="footer">
				<p>Form login | Design by <a href="https://www.facebook.com/dung.ckay">Dũng IT</a></p>
			</div>
		</div>
	</div>

</body>
</html>