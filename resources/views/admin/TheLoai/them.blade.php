@extends('admin')

@section('table-content')
            <!-- /.col-lg-12 -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Thể Loại
                            </h1>
                        </div>
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors)>0)
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $err)
                            {{ $err }}.<br>
                        @endforeach
                    </div>
                @endif

                @if(session('thongbao'))
                    <div class="alert alert-success" role="alert">
                        {{ session('thongbao') }}
                    </div>
                @endif

                <form action={{route('themTheLoai')}} method="POST">
                    <input type="hidden" name="_token" value={{ csrf_token() }} />
                    <div class="form-group">
                        <label>Tên Thể Loại</label>
                        <input class="form-control" name="Ten" placeholder="Nhập tên thể loại" />
                    </div>
                    <button type="submit" class="btn btn-primary">Thêm</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
                    
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
@endsection
