@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">THỂ LOẠI
                    <small>{{$theloai->Ten}}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if(session('thongbao'))
                <div class="alert alert-success" role="alert">
                    {{ session('thongbao') }}
                </div>
            @endif
            <div class="col-lg-7" style="padding-bottom:120px">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <form action={{ route('suaTheLoai',$theloai->id)}} method="POST">

                    <div class="form-group">
                        <label>Tên thể loại</label>
                        <input class="form-control" name="Ten" placeholder="Nhập tên thể loại" value="{{$theloai->Ten}}"/>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Sửa</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection