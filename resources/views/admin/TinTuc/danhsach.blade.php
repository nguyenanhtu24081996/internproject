@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin Tức
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>TieuDe</th>
                        <th>TomTat</th>
                        <th>NoiDung</th>
                        <th>Hinh</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                @foreach ($tintuc as $tt)
                <tbody>
                    <tr class="odd gradeX" align="center">
                        <td>{{$tt->id}}</td>
                        <td>{{$tt->TieuDe}}</td>
                        <td>{{$tt->TomTat}}</td>
                        <td>{{$tt->NoiDung}}</td>
                        <td>{{$tt->Hinh}}</td>
                        <td class="center"><i class="fa fa-trash-o fa-fw"></i><a href={{ route('xoaTinTuc',$tt->id) }}> Delete</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href={{ route('suaTinTuc',$tt->id) }}>Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
</div>
@endsection