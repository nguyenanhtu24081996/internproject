@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nội dung</h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                
                <form action="{{route('themTinTuc')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" id="TheLoai" name="TheLoai">
                            @foreach($theloai as $tl)
                                <option value="{{ $tl->id }}">{{$tl->Ten}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Loại Tin</label>
                        <select class="form-control" id="LoaiTin" >
                        @foreach($loaitin as $lt)
                            <option value="{{ $lt->id }}">{{$lt->Ten}}</option>
                        @endforeach
                    </select>
                    </div>

                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="TieuDe" placeholder="Please Enter Category Name" />
                    </div>
                    <div class="form-group">
                        <label>Tóm Tắt</label>
                        <input class="form-control" name="Tomtat" placeholder="Please Enter Category Order" />
                    </div>
                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="editor1" rows="3" class="form-control ckeditor"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Hình Ảnh</label>
                        <input type="file" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label>Tác Giả</label>
                        <input class="form-control" name="TacGia" placeholder="Please Enter Category Keywords" />
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Thêm</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#TheLoai").change(function(event){
                var idTheLoai = $(this).val();
                console.log(idTheLoai);

                $.get("/internproject/public/admin/ajax/loaitin/"+idTheLoai,function(data){
                    console.log(111)
                    $("#LoaiTin").html(data);
                 });
            });
        }); 
    </script>
@endsection