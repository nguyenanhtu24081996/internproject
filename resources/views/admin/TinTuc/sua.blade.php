@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nội dung
                    <small>{{$tintuc->TieuDe}}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                
                <form action={{ route('suaTinTuc',$tintuc->id) }} method="POST">
                    <input type="hidden" name="_token" value={{ csrf_token() }}>
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" id="TheLoai" name="TheLoai">   
                        
                            @foreach($theloai as $tl)
                            <option 
                                @if($tintuc->loaitin->idTheLoai == $tl->id)
                                {{ "selected" }}
                                @endif
                                value="{{ $tl->id }}">{{$tl->Ten}}</option>
                            @endforeach
                        </select>
                        
                    </div>

                    <div class="form-group">
                        <label>Loại Tin</label>
                        <select class="form-control" id="LoaiTin" >
                            @foreach($loaitin as $lt)
                                <option 
                                    @if($tintuc->idLoaiTin == $lt->id)
                                        {{ "selected" }}
                                    @endif
                                value="{{ $lt->id }}">{{$lt->Ten}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="TieuDe" placeholder="Please Enter Category Name" />{{ $tintuc->TieuDe }}
                    </div>
                    <div class="form-group">
                        <label>Tóm Tắt</label>
                        <input class="form-control" name="Tomtat" placeholder="Please Enter Category Order" />{{ $tintuc->TomTat }}
                    </div>
                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="editor1" rows="3" class="form-control ckeditor"></textarea>{{ $tintuc->NoiDung }}
                    </div>
                    <div class="form-group">
                        <label>Hình Ảnh</label>
                        <p>
                            <img src="upload/tintuc/{{ $tintuc->Hinh }}" width="400px">
                        </p>
                        <input type="file" class="form-control" >{{ $tintuc->Hinh }}
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Sửa</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#TheLoai").change(function(){
                var idTheLoai = $(this).val();
                $.get("/internproject/public/admin/ajax/loaitin/"+idTheLoai,function(data){
                    console.log(111);
                    $("#LoaiTin").html(data);
                 });
            });
        }); 
    </script>
@endsection