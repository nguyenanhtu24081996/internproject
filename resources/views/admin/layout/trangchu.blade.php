@extends('admin.layout.header')

@section('content')
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12">
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                    <a href="#">
                        <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                        <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                    </a>
                    <p class="thoigian">24/02/2020</p>
                    <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                    </p>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                    <ul>
                        <li class="tinmoi">Tin mới</li>
                        <li>
                            <div class="col">
                                <a href="#">
                                    <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-sub"> 
                                    <span class="tomtat-sub"></span>
                                </a>
                                <p class="thoigian">25/02/2020</p>
                            </div>
                        </li>
                        <li>
                            <div class="col">
                                <a href="#">
                                    <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub"> 
                                    <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                </a>
                                <p class="thoigian">25/02/2020</p>
                            </div>
                        </li>
                        <li>
                            <div class="col">
                                <a href="#">
                                    <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                    <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                </a>
                                <p class="thoigian">25/02/2020</p>
                            </div>
                        </li>
                        <li>
                            <div class="col">
                                <a href="#">
                                    <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub"> 
                                    <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                </a>
                                <p class="thoigian">25/02/2020</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                Quảng cáo
            </div>
        </div>
    <!-- Thời Sự -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Thời sự</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Chống tham nhũng </a>|
                                <a href="#" class="link-mini-menu">Quốc hội </a>|
                                <a href="#" class="link-mini-menu"> An Toàn Giao Thông</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!--Kinh Doanh-->
        <div class="catelogy">
            <div class="row">
                <div class="col-lg-7">
                    <div class="catelogy-detail">
                        <p class="t-b">Kinh Doanh</p>
                        <div class="mini-menu">
                            <a href="#" class="link-mini-menu">Tài chính </a>|
                            <a href="#" class="link-mini-menu"> Đầu tư </a>|
                            <a href="#" class="link-mini-menu"> Doanh nhân</a>
                        </div>
                    </div>
                    <hr style="background-color: red;margin-top: 0px;">
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <a href="#">
                            <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                            <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                        </a>
                        <p class="thoigian">24/02/2020</p>
                        <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <ul>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- Giải Trí -->
        <div class="catelogy">
            <div class="row">
                <div class="col-lg-7">
                    <div class="catelogy-detail">
                        <p class="t-b">Giải Trí</p>
                        <div class="mini-menu">
                            <a href="#" class="link-mini-menu">Thế giới Sao </a>|
                            <a href="#" class="link-mini-menu"> Nhạc </a>|
                            <a href="#" class="link-mini-menu"> Phim</a>
                        </div>  
                    </div>
                    <hr style="background-color: red;margin-top: 0px;">
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <a href="#">
                            <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                            <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                        </a>
                        <p class="thoigian">24/02/2020</p>
                        <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <ul>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- Thế Giới -->
        <div class="catelogy">
            <div class="row">
                <div class="col-lg-7">
                    <div class="catelogy-detail">
                        <p class="t-b">Thế Giới</p>
                        <div class="mini-menu">
                            <a href="#" class="link-mini-menu">Bình luận quốc tế </a>|
                            <a href="#" class="link-mini-menu"> Chân dung </a>|
                            <a href="#" class="link-mini-menu"> Hồ sơ</a>
                        </div>
                    </div>
                    <hr style="background-color: red;margin-top: 0px;">
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <a href="#">
                            <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                            <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                        </a>
                        <p class="thoigian">24/02/2020</p>
                        <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <ul>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                            <li>
                                <div class="col">
                                    <a href="#">
                                        <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                        <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                    </a>
                                    <p class="thoigian">25/02/2020</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    <!-- Giáo Dục -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Giáo Dục</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Doanh nhân </a>|
                                <a href="#" class="link-mini-menu"> Tài chính </a>|
                                <a href="#" class="link-mini-menu"> Đầu tư</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!-- Đời Sống -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Đời Sống</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Thời trang </a>|
                                <a href="#" class="link-mini-menu"> Nhạc </a>|
                                <a href="#" class="link-mini-menu"> Phim</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!-- Pháp Luật -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Pháp Luật</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Hồ sơ vụ án </a>|
                                <a href="#" class="link-mini-menu"> Ký sự pháp đình </a>|
                                <a href="#" class="link-mini-menu"> Tư vấn pháp luật</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!-- Thể Thao -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Thể Thao</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Bóng đá Việt Nam </a>|
                                <a href="#" class="link-mini-menu"> Bóng đá quốc tế </a>|
                                <a href="#" class="link-mini-menu"> Các môn khác</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!-- Công Nghệ -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Công Nghệ</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Cộng đồng mạng </a>|
                                <a href="#" class="link-mini-menu"> Ứng dụng </a>|
                                <a href="#" class="link-mini-menu"> Tin công nghệ</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    <!-- Sức Khỏe -->
        <div class="catelogy">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="catelogy-detail">
                            <p class="t-b">Sức Khỏe</p>
                            <div class="mini-menu">
                                <a href="#" class="link-mini-menu">Tư vấn sức khỏe </a>|
                                <a href="#" class="link-mini-menu">Sức khoẻ 24h </a>|
                                <a href="#" class="link-mini-menu"> Các bệnh</a>
                            </div>
                        </div>
                        <hr style="background-color: red;margin-top: 0px;">
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <a href="#">
                                <img src="../public/image/image_dev_ngoc/gia-pho-cuc-truong-cua-bo-cong-an-den-cong-an-huyen-tham-anh-em-360x240.jpg" alt="" class="hinhanh-main">
                                <p class="tomtat">Tiết lộ khó tin vụ đại tá giả đến thăm công an thật ở Hậu Giang</p>
                            </a>
                            <p class="thoigian">24/02/2020</p>
                            <p class="noidung">Đối tượng giả Phó cục trưởng Cục cơ yếu - Bộ Công an đến thăm công an thật ở Hậu Giang thừa nhận đã lừa đảo hơn 6 tỷ đồng.
                            </p>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                            <ul>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <a href="#">
                                            <img src="../public/image/image_dev_ngoc/kien-nghi-kiem-diem-chu-tich-cac-pho-chu-tich-tinh-quang-ngai-1-140x93.jpg" alt="" class="hinhanh-sub">
                                            <span class="tomtat-sub">Kiến nghị kiểm điểm cựu Chủ tịch tỉnh Quảng Trị Nguyễn</span>
                                        </a>
                                        <p class="thoigian">25/02/2020</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
</div>
@endsection

