@extends('admin')

@section('table-content')
            <!-- /.col-lg-12 -->
            
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Tên Loại Tin</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($loaitin as $lt)
                    <tr class="odd gradeX" align="center">
                        <td>{{$lt->id}}</td>
                        <td>{{$lt->Ten}}</td>
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href={{route('xoaLoaiTin',$lt->id)}}>Delete</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href={{route('suaLoaiTin',$lt->id)}}>Edit</a></td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection
