@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Loại Tin
                    <small>Add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <form action={{route('themLoaiTin')}} method="POST">
                    
                    <div class="form-group">
                        <label>Tên Loại Tin</label>
                        <input class="form-control" name="TenLoaiTin" placeholder="Nhập tên loại tin" />
                    </div>

                    <div class="form-group">
                        <label>ID Thể Loại</label>
                        <input class="form-control" name="TenLoaiTin" placeholder="Nhập ID thể loại" />
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Thêm</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection
