@extends('admin')

@section('table-content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">LOẠI TIN
                    <small></small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <form action={{route('suaLoaiTin',$loaitin->id)}} method="POST">
                    <div class="form-group">
                        <label>Tên loại tin</label>
                        <input class="form-control" name="TenTheLoai" placeholder="Nhập tên thể loại" value="{{ $loaitin->Ten }}"/>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Sửa</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection
