<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/noidung.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    @extends('layouts.footer')
    @extends('layouts.header')
    @section('header')
        @parent
        
<div class="container">        
        <div class="left">
            <div class="logo clearfix">
                <div class="logo1"><a href="#">THỜI SỰ</a></div>
                <div class="logo2"><a href="#"><span>></span>CHÍNH TRỊ</a></div>
              <hr>  
            </div>
            <div class="title"><h1><b>Ông Vương Đình Huệ: Không để người tham nhũng thoát ra, lọt vào cấp uỷ</b></h1></div>
            <div class="news">
                <span>Tin mới</span>
                <b>"Không để những người tiêu cực, tham nhũng rồi có khuyết điểm nghiêm trọng lại thoát ra, lọt vào cấp ủy các cấp", Bí thư Hà Nội Vương Đình Huệ nói.</b>              
            </div>
            <div class="item">
            <ul style="list-style-type:square">
                <li><a href="#">Phê chuẩn ông Vương Đình Huệ làm trưởng đoàn ĐBQH Hà Nội</a></li>
                <li><a href="#">Chuẩn bị Đại hội Đảng 13: Công tác cán bộ làm không tốt thì tự ta lật đổ ta</a></li>
                <li><a href="#">Đại hội 13 sẽ bàn về '4 không' trong phòng chống tham nhũng</a></li>
            </ul>
            </div>          
                <p>
                    Sáng nay, Bí thư Thành uỷ Hà Nội Vương Đình Huệ có cuộc làm việc với UB MTTQ Việt Nam TP Hà Nội.
                </p>
                <br>           
                <p>   
                    Theo ông, cuộc làm việc là dịp để lãnh đạo TP trực tiếp lắng nghe, thấu hiểu đầy đủ, sâu sắc hơn
                 những tâm tư nguyện vọng của các tầng lớp nhân dân Thủ đô để kịp thời lãnh đạo, chỉ đạo giải quyết. Ông cũng chia sẻ vui rằng, từ bé đến giờ ông mới được gặp trực tiếp nghệ sĩ Tự Long, Lan Hương khi 2 nghệ sĩ tới dự cuộc gặp này.</p>
                <div>
                            <img src="../public/image/image_dev_huan/ong-vuong-dinh-hue-khong-de-nguoi-tham-nhung-thoat-ra-lot-vao-cap-uy.jpg" alt="">
                </div 
                <p>
                Tân Bí thư Hà Nội nhấn mạnh một số kết quả nổi bật của MTTQ TP như đã chủ động tuyên truyền, nâng cao nhận thức và ý thức phòng chống dịch bệnh trong nhân dân, xã hội hoá nguồn lực, phát miễn phí các trang thiết bị bảo vệ sức khoẻ nhân dân, nhất là cho người nghèo.
                </p>
                <br>
                <p>
                        Ông Vương Đình Huệ cho hay, ban chỉ đạo phòng chống dịch của TP cũng hành động rất quyết liệt, xác định bảo vệ được Hà Nội là bảo vệ được cho cả nước, trách nhiệm này rất nặng nề.
                </p>
                <br>
            <p>
                Bí thư Hà Nội thông tin, tối qua TP đã bắt đầu tiếp nhận một số công dân Việt Nam và một số người Hàn Quốc đi từ vùng dịch Covid-19 về trường quân sự của Thủ đô để cách ly.</p>
            <br>
            <p>
                Đánh giá cao những kết quả đạt được của UB MTTQ TP, song theo Bí thư Vương Đình Huệ, còn một số hạn chế trong các hoạt động của MTTQ TP cần khắc phục sớm.</p>
            <br> 
            <p>
                <strong style="color: #333339;">Không sợ ùn, chỉ sợ tắc</strong>
            </p><br>
            <p>
                Nhấn mạnh 2020 là năm có nhiều sự kiện trọng đại của đất nước và Thủ đô, Bí thư Hà Nội đề nghị MTTQ TP quan tâm thực hiện một số nhiệm vụ.
            </p>
            <div class="z"><img src="../public/image/image_dev_huan/bi-thu-thanh-uy-ha-noi-vuong-dinh-hue-va-cac-nghe-si-lan-huong-tu-long-xuan-bac-anh-quang-vinh.jpg"></div>
            <p>
                Cụ thể, MTTQ các cấp của TP tổ chức giám sát theo đúng chức năng, quy định của pháp luật, lựa chọn có trọng tâm trọng điểm các nội dung giám sát, xây dựng Đảng và chính quyền vững mạnh.
            </p><br>
            <p>
                “Chúng ta cần nhận thức rằng không có lực lượng giám sát nào hùng hậu, toàn diện và sâu sắc bằng nhân dân.
            </p><br>
            <p>
                Giám sát và phản biện xã hội để tăng thêm sự đoàn kết thống nhất, đồng thuận xã hội, phát huy tinh thần sáng tạo của nhân dân và DN”, ông Huệ nói.
            </p><br>
            <p>
                Lãnh đạo Thành uỷ lưu ý việc MTTQ cần tích cực tham gia kiểm soát quyền lực, giám sát để quản lý xã hội một cách công khai và minh bạch.
            </p>
            <div class="z"><img src="../public/image/image_dev_huan/ong-vuong-dinh-hue-khong-de-nguoi-tham-nhung-thoat-ra-lot-vao-cap-uy-1.jpg" ></div>
            <p>
                Mặt trận phải là nơi để người dân gửi gắm niềm tin và phản ánh tố giác tội phạm tham nhũng, tố giác cán bộ đảng viên có biểu hiện suy thoái, tiêu cực, nhũng nhiễu nhân dân, góp phần làm trong sạch bộ máy và đội ngũ cán bộ đảng viên.
            </p><br>            
            <p>
                Ngoài ra, Mặt trận cần kịp thời nắm bắt tâm tư, nguyện vọng của nhân dân để tham mưu cấp uỷ lãnh đạo, chỉ đạo, giải quyết kịp thời kiến nghị chính đáng của nhân dân.
            </p><br>
            <p>
                Theo ông, phải tìm hiểu xem những vấn đề bức xúc của người dân hiện nay là gì. Ông dẫn chứng như tình trạng ùn tắc, tai nạn giao thông, ý thức của người dân khi tham gia giao thông.
            </p><br>
            <p>
                “Một số TP như TP.HCM ít có trường hợp đường đang ùn tắc mà xe máy trèo lên vỉa hè. Chúng ta không sợ ùn mà chỉ sợ tắc. Nói để xem những việc nhỏ như vậy chúng ta có làm được không”, ông Vương Đình Huệ nêu.
            </p><br>
            <p>
                Bí thư Hà Nội cho hay, Thành uỷ đang chỉ đạo rất quyết liệt các vụ việc liên quan đến tiếp dân và giải quyết khiếu nại tố cáo, xây dựng tổ chức cơ sở đảng, sàng lọc để đảm bảo nhân sự tham gia cấp uỷ.
            </p><br>

            <p> “Không để những người tiêu cực, tham nhũng rồi có khuyết điểm nghiêm trọng lại thoát ra, lọt vào cấp uỷ các cấp”, lãnh đạo Thành uỷ nhấn mạnh.</p><br>
                
            <p>   Về kiến nghị công tác xây dựng Đảng, ông Vương Đình Huệ đề nghị các cấp uỷ rà soát lại hơn nữa thể chế quản lý, giám sát cán bộ, đảng viên, chú trọng phát huy và tạo điều kiện của nhân dân, MTTQ, các thành viên giám sát công tác cán bộ, kiểm soát quyền lực, công tác phòng chống tham nhũng lãng phí, xử lý nghiêm cán bộ, đảng viên có vi phạm.</p><br>
                
            <p>    Liên quan đến đường sắt đô thị Cát Linh - Hà Đông, Bí thư Hà Nội cho biết đã chỉ đạo UBND TP phối hợp với Bộ GTVT tổ chức hội nghị liên tịch sắp tới để có kiến nghị giải quyết theo thẩm quyền, cái gì vượt thẩm quyền thì báo cáo Thủ tướng và Chính phủ, sớm đưa dự án này vào hoạt động.</p><br>
            
            <p>Thành uỷ cũng chỉ đạo TP họp với Bộ TN&MT và các cơ quan liên quan để có giải pháp căn cơ giải quyết tình trạng ô nhiễm môi trường. </p><br>
        </div> 
            <p><span class="bold">Hương Quỳnh</span></p>
        </div>
        <div class>

        </div>         
    @endsection
</body>
</html>