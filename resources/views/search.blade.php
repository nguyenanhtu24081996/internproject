<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/searchpage.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    @extends('layouts.footer')
    @extends('layouts.header')
    @section('header')
        @parent
        <div class="searchpage">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="clearfix">
                            <div class="result">
                                <p>10 kết quả tìm kiếm theo từ khóa:a</p>
                                <hr>
                            </div>
                            <div class="result-title">
                                <div class="link-title">
                                    <a href="#"><p>Cô gái trở về từ vùng dịch Hàn Quốc có dấu hiệu sốt, cách ly thêm 16 nguời</p></a>
                                </div>
                                <div class="table-result">
                                    <img src="../public/image/image_dev_tu/Logo.png">
                                    <div class="detail">
                                        <p>Đến chiều 26/2, cơ quan chức năng tỉnh Quảng Trị đã cách ly, theo dõi 2 người trở về từ vùng dịch ở Hàn Quốc, một cô gái đã có biểu hiện sốt.</p>
                                        <p>(27/02/2020)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="result-title">
                                <div class="link-title">
                                    <a href="#"><p>Cô gái trở về từ vùng dịch Hàn Quốc có dấu hiệu sốt, cách ly thêm 16 nguời</p></a>
                                </div>
                                <div class="table-result">
                                    <img src="../public/image/image_dev_tu/Logo.png">
                                    <div class="detail">
                                        <p>Đến chiều 26/2, cơ quan chức năng tỉnh Quảng Trị đã cách ly, theo dõi 2 người trở về từ vùng dịch ở Hàn Quốc, một cô gái đã có biểu hiện sốt.</p>
                                        <p>(27/02/2020)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="result-title">
                                <div class="link-title">
                                    <a href="#"><p>Cô gái trở về từ vùng dịch Hàn Quốc có dấu hiệu sốt, cách ly thêm 16 nguời</p></a>
                                </div>
                                <div class="table-result">
                                    <img src="../public/image/image_dev_tu/Logo.png">
                                    <div class="detail">
                                        <p>Đến chiều 26/2, cơ quan chức năng tỉnh Quảng Trị đã cách ly, theo dõi 2 người trở về từ vùng dịch ở Hàn Quốc, một cô gái đã có biểu hiện sốt.</p>
                                        <p>(27/02/2020)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="result-title">
                                <div class="link-title">
                                    <a href="#"><p>Cô gái trở về từ vùng dịch Hàn Quốc có dấu hiệu sốt, cách ly thêm 16 nguời</p></a>
                                </div>
                                <div class="table-result">
                                    <img src="../public/image/image_dev_tu/Logo.png">
                                    <div class="detail">
                                        <p>Đến chiều 26/2, cơ quan chức năng tỉnh Quảng Trị đã cách ly, theo dõi 2 người trở về từ vùng dịch ở Hàn Quốc, một cô gái đã có biểu hiện sốt.</p>
                                        <p>(27/02/2020)</p>
                                    </div>
                                </div>
                            </div>
                            <div class="result-title">
                                <div class="link-title">
                                    <a href="#"><p>Cô gái trở về từ vùng dịch Hàn Quốc có dấu hiệu sốt, cách ly thêm 16 nguời</p></a>
                                </div>
                                <div class="table-result">
                                    <img src="../public/image/image_dev_tu/Logo.png">
                                    <div class="detail">
                                        <p>Đến chiều 26/2, cơ quan chức năng tỉnh Quảng Trị đã cách ly, theo dõi 2 người trở về từ vùng dịch ở Hàn Quốc, một cô gái đã có biểu hiện sốt.</p>
                                        <p>(27/02/2020)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul class="pagination">
                                <li class="page-item disabled"><a class="page-link" href="#"><</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
</body>
</html>
