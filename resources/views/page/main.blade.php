<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/header_footer.css">
    <link rel="stylesheet" href="../public/css/style_main.css" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <base href="{{asset('')}}">
</head>

<body>
        @extends('admin.layout.header') 
        @section('content')
        @foreach($theloai as $tl)
        <div class="container">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="catelogy">
                        <div class="row">
                            <div class="col-lg-7">
                                    @if(count($tl->loaitin)>0)
                                    <div class="catelogy-detail">
                                        <p class="t-b">{{$tl->Ten}}</p>
                                        <div class="mini-menu">
                                            @foreach($tl->loaitin as $lt)
                                            <a href="#" class="link-mini-menu">{{ $lt->Ten }}</a>|
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr style="background-color: red;margin-top: 0px;">
                                @endif
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                        <?php
                                            $data = $tl->tintuc->take(5);
                                            $tin1 = $data->shift();
                                        ?>
                                            <a href="#">
                                                <img src="../public/image/image_dev_ngoc/{{$tin1['Hinh']}}" alt="" width="300px" class="hinhanh-main">
        
                                                <p class="tomtat">{{ $tin1['TieuDe'] }}</p>
                                            </a>
                                            {{-- <p class="thoigian">24/02/2020</p> --}}
                                            <p class="noidung">{{ $tin1['TomTat'] }}
                                            </p>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                            <ul>
                                                <li>
                                                        @foreach($data->all() as $tintuc)
                                                        <div class="small-news">
                                                            <a href="#">
                                                                <img src="../public/image/image_dev_ngoc/{{$tintuc['Hinh']}}" alt="" class="hinhanh-sub">
                                                                <p class="tomtat-sub">{{$tintuc['TieuDe']}}</p>
                                                            </a>
                                                        </div>
                                                        @endforeach
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                             
                            </div>
                        </div>
                       
                </div>
               
            </div>
        </div>
        @endforeach
        @endsection
        
        {{--  @include('admin.layout.footer')  --}}
</body>

</html>