<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

use Illuminate\Support\Facades\Route;
use App\TheLoai;
use Monolog\Handler\RotatingFileHandler;

// use App\LoaiTin;
// use App\TinTuc;

Route::get('/', function () {
    return view('main');
});
Route::get('search', function () {
    return view('search');
});
Route::get('login', function () {
    return view('login');
});

// Route::get('main', function(){
//     return view('main');

Route::get('noidung', function () {
    return view('noidung');
});

Route::get('admin', function () {
    return view('admin');
});

Route::get('catelogy', function () {
    return view('catelogy');
});

// Route::get('test', function () {
//     return view('index');
// });

Route::get('test', function() {
    $theloai = TheLoai::find(1);
    foreach($theloai->LoaiTin as $loaitin){
        echo $loaitin->TenLoaiTin."<br>";
    }
});


Route::group(['prefix' => 'admin'], function () {

    Route::group(['prefix' => 'theloai'], function () {
        Route::get('danhsach','TheLoaiController@getDanhSach');

        Route::get('sua/{id}','TheLoaiController@getSua')->name('suaTheLoai');
        Route::post('sua/{id}','TheLoaiController@postSua');

        Route::get('them','TheLoaiController@getThem');
        Route::post('them','TheLoaiController@postThem')->name('themTheLoai');

        Route::get('xoa/{id}','TheLoaiController@getXoa')->name('xoaTheLoai');
    });

    Route::group(['prefix' => 'loaitin'], function () {
        Route::get('danhsach','LoaiTinController@getDanhSach');

        Route::get('sua/{id}','LoaiTinController@getSua')->name('suaLoaiTin');
        Route::post('sua/{id}','LoaiTinController@postSua');

        Route::get('them','LoaiTinController@getThem');
        Route::post('them','LoaiTinController@postThem')->name('themLoaiTin');

        Route::get('xoa/{id}','LoaiTinController@getXoa')->name('xoaLoaiTin');

    });

    Route::group(['prefix' => 'tintuc'], function () {
        Route::get('danhsach','TinTucController@getDanhSach');

        Route::get('sua/{id}','TinTucController@getSua')->name('suaTinTuc');
        Route::post('sua/{id}','TinTucController@postSua');

        Route::get('them','TinTucController@getThem');      
        Route::post('them','TinTucController@postThem')->name('themTinTuc');

        Route::get('xoa/{id}','TinTucController@getXoa')->name('xoaTinTuc');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin')->name('ajaxTintuc');
    });
});

// Route::get('test', function () {
//     return view('test');
// });



    
// });

Route::get('trangchu','PagesController@trangchu');

