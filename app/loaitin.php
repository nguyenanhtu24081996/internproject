<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTin extends Model
{
    protected $table = "loaitin";

    public function tintuc(){
        return $this->hasMany('App\TinTuc','idLoaiTin','id');
    }

    public function theloai(){
        return $this->belongsToMany('App\TheLoai','idTheLoai','id');
    }
}
