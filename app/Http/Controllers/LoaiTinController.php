<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoaiTin;
class LoaiTinController extends Controller
{
    public function getDanhSach(){
        $loaitin = LoaiTin::all();

        return view('admin.loaitin.danhsach',compact('loaitin'));
    }

    public function getSua($id){
        $loaitin = LoaiTin::find($id);

        return view('admin.loaitin.sua',compact('loaitin'));
    }

    public function postSua(Request $request , $id){
        $loaitin = LoaiTin::find($id);
        $this->validate($request,[
            'Ten' => 'required|unique:loaitin,Ten|min:3|max:100'
        ],[
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.unique'=>'Tên thể loại đã tồn tại',
            'Ten.min'=>'Ten the loai phai tu 3 den 100 ky tu',
            'Ten.max'=>'Ten the loai phai tu 3 den 100 ky tu'
        ]);
        $loaitin->Ten = $request->Ten;
        $loaitin->save();

        return redirect('admin/loaitin/sua/'.$id)->with('thongbao','Sửa Thành Công');
    }

    public function getThem(){
        return view('admin.loaitin.them');
    }

    public function postThem(Request $request){
        

        $this->validate($request,[
            'Ten' =>'required|min:3|max:100'
        ],[
            'Ten.required' =>'Bạn chưa nhập tên thể loại',
            'Ten.min' => 'Tên thể loại phải từ 3 ký tự đến 100 ký tự',
            'Ten.max' => 'Tên thể loại phải từ 3 ký tự đến 100 ký tự'
        ]);
        
        $table = new LoaiTin;
        $table->Ten  = $request->Ten;
        $table->save();

        return redirect('admin/loaitin/them')->with('thongbao','Thêm Thành Công');
    }

    public function getXoa($id){
        $theloai = LoaiTin::find($id);

        $theloai->delete();
        return redirect('admin/loaitin/danhsach')->with('thongbao','Bạn đã xóa thành công');
    }
}

