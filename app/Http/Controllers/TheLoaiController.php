<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;

class TheLoaiController extends Controller
{
    public function getDanhSach(){
        $theloai = TheLoai::all();

        return view('admin.theloai.danhsach',compact('theloai'));
    }

    public function getSua($id){
        
        $theloai = TheLoai::find($id);
       
        return view('admin.theloai.sua',compact('theloai'));
    }

    public function postSua(Request $request , $id){
        $theloai = TheLoai::find($id);
        $this->validate($request,[
            'Ten' => 'required|unique:theloai,Ten|min:3|max:100'
        ],[
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.unique'=>'Tên thể loại đã tồn tại',
            'Ten.min'=>'Ten the loai phai tu 3 den 100 ky tu',
            'Ten.max'=>'Ten the loai phai tu 3 den 100 ky tu'
        ]);
        $theloai->Ten = $request->Ten;
        $theloai->save();

        return redirect('admin/theloai/sua/'.$id)->with('thongbao','Sửa Thành Công');
    }

    public function getThem(){
        return view('admin.theloai.them');
    }

    public function postThem(Request $request){
        // dd(1);
        $this->validate($request,[
            'Ten' =>'required|min:3|max:100'
        ],[
            'Ten.required' =>'Bạn chưa nhập tên thể loại',
            'Ten.min' => 'Tên thể loại phải từ 3 ký tự đến 100 ký tự',
            'Ten.max' => 'Tên thể loại phải từ 3 ký tự đến 100 ký tự'
        ]);
        
        $table = new TheLoai;
        $table->Ten  = $request->Ten;
        $table->save();
        return redirect('admin/theloai/danhsach')->with('thongbao','Thêm Thành Công');
    }

    public function getXoa($id){
        $theloai = TheLoai::find($id);

        $theloai->delete();
        return redirect('admin/theloai/danhsach')->with('thongbao','Bạn đã xóa thành công');
    }
}

