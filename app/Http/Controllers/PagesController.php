<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
use App\TinTuc;

class PagesController extends Controller
{
    function __construct()
    {
        $theloai = TheLoai::all();
        view()->share('theloai',$theloai);
    }
    public function trangchu(){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc = TinTuc::all();

        return view('page.main',compact('theloai','loaitin','tintuc'));
    }
}
