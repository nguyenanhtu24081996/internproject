<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TinTuc;
use App\TheLoai;
use App\LoaiTin;
class TinTucController extends Controller
{
    public function getDanhSach(){
        $tintuc = TinTuc::all();
        
        return view('admin.tintuc.danhsach',['tintuc'=>$tintuc]);
    }

    public function getSua($id){

        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc = TinTuc::with('loaitin')->where('id',$id)->first();
        return view('admin.tintuc.sua',compact('tintuc','theloai','loaitin'));
    }

    public function postSua($id){

    }

    public function getXoa(){
        //
    }

    public function getThem(){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();

        return view('admin.tintuc.them',compact('theloai','loaitin'));
    }

    public function postThem(Request $request){
        $this->validate($request,[
            'LoaiTin'=> 'required',
            'TieuDe'=>'required|min:3|unique:TinTuc,TieuDe',
            'TomTat'=> 'required',
            'NoiDung'=> 'required'
        ],[
            'LoaiTin.required'=> 'Bạn chưa nhập loại tin',
            'TieuDe.required'=> 'Bạn chưa nhập tiêu đề ',
            'TieuDe.unique'=> 'Tiêu đề đã tồn tại',
            'TomTat.required'=> 'Bạn chưa nhập tóm tắt ',
            'NoiDung.required'=> 'Bạn chưa nhập nội dung '
        ]);

        $tintuc = new TinTuc;
        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->NoiDung = $request->NoiDung;

        if($request->hasFile('Hinh')){
            $file = $request->file('Hinh');

            $name = $file->getClientOriginalName();
            $Hinh = str_random(4)."_".$name;
            while(file_exists("upload/tintuc".$Hinh)){
                $Hinh = str_random(4)."_".$name;
            };
            $file->move("upload/tintuc",$Hinh);
            $tintuc->Hinh = $Hinh;

            return redirect('admin/tintuc/them')->with('thongbao','Them thanh cong');
        }else
        {
            $tintuc->Hinh = "";
        }
    }
}
