<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    protected $table = "tintuc";

    public function loaitin(){
        return $this->hasOne('App\LoaiTin','id','idLoaiTin');
    }

}
